#!/bin/bash
echo "Version 1 - 2022-07-25-16:17:00"
sleep 3

cur_dir="$( dirname -- "$0"; )";
who_am_i="$( whoami; )";


echo "########################"
echo "# Updating your system #"
echo "########################"

sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt-get install libmicrohttpd-dev sqlite3 libsqlite3-dev default-libmysqlclient-dev libpq-dev libgnutls28-dev libconfig-dev libldap2-dev liboath-dev libcbor-dev libsystemd-dev libjansson-dev libcurl4-gnutls-dev cmake -y


echo "##########################"
echo "# Make working directory #"
echo "##########################"

mkdir peerid-sso
cd peerid-sso || return


echo "##########################################################################################"
echo "# Install Orcania - C Library for different purposes that can be shared among C programs #"
echo "##########################################################################################"

git clone https://github.com/babelouest/orcania.git
cd orcania/src/ || return
make
sudo make install
cd ../.. || return

echo "####################################"
echo "# Install Yder - C Logging library #"
echo "####################################"

git clone https://github.com/babelouest/yder.git
cd yder/src/ || return
make # Or make Y_DISABLE_JOURNALD=1 to disable journald logging
sudo make install
cd ../.. || return

echo "#########################################"
echo "# Install Ulfius - C Rest API Framework #"
echo "#########################################"

git clone https://github.com/babelouest/ulfius.git
cd ulfius/src/ || return
make
sudo make install
cd ../.. || return

echo "###################################################################################################"
echo "# Install Hoel - Abstraction library written in C. Simple and easy to use database access library #"
echo "###################################################################################################"

git clone https://github.com/babelouest/hoel.git
cd hoel/src/ || return
make
sudo make install
cd ../.. || return

echo "############################################################################################################"
echo "# Install Rhonabwy - Javascript Object Signing and Encryption (JOSE) library - JWK, JWKS, JWS, JWE and JWT #"
echo "############################################################################################################"

git clone https://github.com/babelouest/rhonabwy.git
cd rhonabwy/src/ || return
make
sudo make install
cd ../.. || return

echo "##############################################################################################################################"
echo "# Install Iddawc  - C library used to implement OAuth2/OIDC clients according to the OAuth2 RFC and the OpenID Connect Specs #"
echo "##############################################################################################################################"

git clone https://github.com/babelouest/iddawc.git
cd iddawc/src/ || return
make
sudo make install
cd ../.. || return

echo "###########################################"
echo "#Installing further Glewlwyd dependencies #"
echo "###########################################"

sudo apt install libevent-dev check libuv1-dev -y
sudo apt install cmake-curses-gui -y

echo "##########################################################################################################################################################################"
echo "# Install Glewlwyd - Single Sign On server, OAuth2, Openid Connect, multiple factor authentication with, HOTP/TOTP, FIDO2, TLS Certificates, etc. extensible via plugins #"
echo "##########################################################################################################################################################################"


#Orginal Repo:
#git clone https://github.com/babelouest/glewlwyd.git

git clone --recursive https://gitlab.com/PBSA/peerplays-sso/Peerplays-SSO-Backend.git
cd Peerplays-SSO-Backend/ || return
mkdir build
cd build || return
cmake ..
make -j 4
sudo make install
cd .. || return

cd build || return
sudo chmod -R a+rwx glewlwyd || return
cd .. || return

echo "###########################"
echo "## Installing Postgres-14 #"
echo "###########################"

sudo wget -qO /etc/apt/trusted.gpg.d/pgdg.asc https://www.postgresql.org/media/keys/ACCC4CF8.asc
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -sc)-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
sudo apt update
sudo apt install -y postgresql-14

echo "################################################"
echo "# Checking versioning /  System service status #"
echo "################################################"
pg_config --version
sudo service postgresql status


echo "###################################"
echo "# Configuring/Optimizing Postgres #"
echo "####################################"

echo "~~MAKE SURE FOLLOWING COMMANDS ARE EDITED~~"
echo "IF YOU DID NOT MAKE EDITS, YOU MUST NOW!"
sleep 5
sudo -u postgres psql -c "ALTER USER postgres with password 'CHANGEME';"
sudo sed -i -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/14/main/postgresql.conf


echo 'host all all 0.0.0.0/0 scram-sha-256' | sudo tee -a /etc/postgresql/14/main/pg_hba.conf
echo 'host all all ::/0 scram-sha-256' | sudo tee -a /etc/postgresql/14/main/pg_hba.conf
echo 'local   all             glewlwyd                                trust' | sudo tee -a /etc/postgresql/14/main/pg_hba.conf

echo "################################"
echo "#Configuring Glewlwyd Database #"
echo "################################"

echo "~~MAKE SURE FOLLOWING COMMANDS ARE EDITED~~"
echo "IF YOU DID NOT MAKE EDITS, YOU MUST NOW!"
sleep 5

sudo -u postgres psql -tc "SELECT 1 FROM pg_user WHERE usename = 'glewlwyd'" | grep -q 1 | sudo -u postgres psql -c "CREATE role glewlwyd login password 'CHANGEME';"
sudo -u postgres psql -tc "SELECT 1 FROM pg_database WHERE datname = 'glewlwyd'" | grep -q 1 | sudo -u postgres psql -c "CREATE DATABASE glewlwyd owner glewlwyd"
sudo -u postgres psql -c "grant connect on database glewlwyd to glewlwyd;"
sudo -u postgres psql -d glewlwyd -c "create extension pgcrypto;"

sudo -u postgres  psql -h localhost -U glewlwyd -d glewlwyd < docs/database/init.postgre.sql 


echo "######################"
echo "# Adding PeerID User #"
echo "######################"
sudo useradd -m -p testingsso -s /bin/bash peerid

echo "########################"
echo "# Configuring Glewlwyd #"
echo "########################"
cp -v docs/glewlwyd.conf.sample build/glewlwyd.conf
